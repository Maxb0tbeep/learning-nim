import std/strutils

echo "calculator! what operation? (+, -, *, /)"
let operation:string = readLine(stdin)
echo "what is the first number?"
let num1:float = parseFloat(readLine(stdin))
echo "what is the second number?"
let num2:float = parseFloat(readLine(stdin))

if operation == "+":
  echo num1+num2
elif operation == "-":
  echo num1-num2
elif operation == "*":
  echo num1*num2
elif operation == "/":
  echo num1/num2
else:
   echo "Invalid operation"
